;
(function() {
		$.extend({
			/**
			 * 滑动删除
			 * @param {Object} className 滑动区域元素样式名称
			 * @param {Object} leftFn 向左滑动回调函数
			 * @param {Object} rightFn 向右滑动回调函数
			 * @example 
			 * $.leftToDel(".s-cmdy-des",function(){// left code},function(){ // right code});
			 */
			leftToDel: function(className, leftFn, rightFn) {
				$('body').on('touchstart', className, function(e) {
					var _self = $(this),
						touch = e.originalEvent,
						startX = touch.changedTouches[0].pageX,
						startY = touch.changedTouches[0].pageY;
					_self.on('touchmove', function(e) {
						e.preventDefault();
						touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
						if(touch.pageX - startX < -50) { // 左滑
							_self.off('touchmove');
							leftFn(_self);
					        return false;
						};
						if(touch.pageX - startX > 50) { // 右滑
							_self.off('touchmove');
							rightFn(_self);
							return false;
						}
					});
				}).on('touchend', function() {
					$(this).off('touchmove');
				});
			}
		});
})();