/**
 * 
 */
;
(function(comm) {
	var dialog = window.YDUI.dialog;

	/**
	 * 获取uuid，默认为5位数
	 * @param {Number} n  [正整数，取值区间5-25]
	 */
	comm.uuid = function(n) {
		return Math.random().toString(36).substring(2, n > 5 ? n + 2 : 7);
	};


	/**
	 * 设置cookie值
	 * @param {String} key  [设置的唯一标识]
	 * @param {Object} value  [需要设置的值]
	 * @param {Object} options  [参数集合以对象形式存储]
	 * @example
	 * 1、设置  TSH.Cookie('test',value);
	 * 2、获取  TSH.Cookie('test');
	 * 3、删除  TSH.Cookie('test',null);
	 * 4、详细设置 TSH.cookie("key","value",{expire:"xx",domain:"xx",path:"xx",unit: "xx"})
	 */
	comm.Cookie = function(key, value, options) {
		if(arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value == null)) {
			options = options || {};
			if(value == null) {
				options.expires = -1;
			}

			if(typeof options.expires === 'number') {
				var days = options.expires,
					t = options.expires = new Date();
				t.setDate(t.getDate() + days);
			}
			value = String(value);
			options.path = '/';
			return(document.cookie = [encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value), options.expires ? '; expires=' + options.expires.toUTCString() : '', options.path ? '; path=' + options.path : '', options.domain ? '; domain=' + options.domain : '', options.secure ? '; secure' : ''].join(''));
		}
		options = value || {};
		var decode = options.raw ? function(s) {
			return s;
		} : decodeURIComponent;
		var pairs = document.cookie.split('; ');
		for(var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
			if(decode(pair[0]) === key)
				return decode(pair[1] || '');
		}
		return null;
	};

	/**
	 * HTML5存储
	 * @param {String} key
	 * @param {Object} value
	 * @param {Boolean} isDel
	 * 1、设置  TSH.storage('test',{"name":"test"});
	 * 2、获取  TSH.storage('test');
	 * 3、删除  TSH.storage('test','');
	 */
	comm.storage = function(key, value, isDel) {
		if(!key || (!sessionStorage && !localStorage)) {
			return null;
		}
		if(!sessionStorage) {
			sessionStorage = localStorage;
			window.onbeforeunload = function() {
				sessionStorage.clear();
			};
		}
		//清空sessionStorage的值
		if(isDel) {
			sessionStorage.removeItem(key);
			return null;
		}
		//如果value不存在；返回sessionStorage里的参数
		if(!value) {
			var r = decodeURI(sessionStorage.getItem(key));
			if(r.indexOf("{") != -1 && r.indexOf("}"))
				r = JSON.parse(r);
			return r;
		}
		//value存在，将键值对设置sessionStorage
		else {
			if(typeof value !== 'string') {
				value = encodeURI(JSON.stringify(value));
			}
			sessionStorage.setItem(key, value);
			return 1;
		}
	};

	/**
	 * 判断对象是否是数组
	 * @param {Object} o
	 */
	comm.isArray = function(o) {
		return o && o.length ? true : false;
	};

	/**
	 * AES 加密
	 * @param {String} txt
	 */
	comm.aes = function(txt) {
		var key = CryptoJS.enc.Utf8.parse("!@#987321ascwfds"); // aes 加密秘钥与服务器一致
		var encryptedData = CryptoJS.AES.encrypt(txt, key, {
			iv: key,
			mode: CryptoJS.mode.CBC,
			padding: CryptoJS.pad.Pkcs7
		});
		return encryptedData.toString();
	}

	/**
	 * 获取url指定名称的参数
	 * @param {String} name
	 */
	comm.getParm = function(name) {
		var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
		var r = window.location.search.substr(1).match(reg);
		if(r) {
			return decodeURI(r[2]);
		}
		return null;
	}

	/**
	 * 判断是否登录
	 */
	comm.isLogin = function() {
		comm.token();
		return;
		if(comm.storage("login") === "null" || comm.storage("login").token == "") {
			// 判断当前页面是不是在过滤列表中，不在过滤列表中的页面会进行登录验证
			if(!comm.isContain(['login.html', 'regist.html', 'forget.html'], location.pathname.split('/'))) {
				comm.toView('login.html');
			}
		} else {
			// 已登录，不能访问注册、登录、忘记密码
			if(comm.isContain(['login.html', 'regist.html', 'forget.html'], location.pathname.split('/'))) {
				comm.toView('home.html');
			}
		}
	}
	/**
	 * 跳转到指定页面
	 * @param {String} url
	 */
	comm.toView = function(url) {
		window.location.href = url;
	}

	/**
	 * 日志输出统一管理方便控制
	 * @param {Object} txt
	 */
	comm.log = function(txt) {
		// 在非生产环境下生效
		if(!ZXX.cfg.isPrdu){
		   console.log(txt);	
		}
	}
	/**
	 * 判断a字符串是否包含在b字符串中
	 * @param {Array||String} a 过滤列表，可以不用验证登录
	 * @param {String} b
	 */
	comm.isContain = function(a, b) {
		a = a.length === 1 ? a.split(',') : a;
		var bool = false;
		$.each(a, function(i, v) {
			if($.inArray(v, b) > -1) {
				bool = true;
				return;
			}
		});
		return bool;
	}
	/**
	 * 发送短信验证码
	 * @param {Object} obj 发送短信按钮jquery对象
	 * @param {Function} fn 回调函数 
	 * @example 注册页面发送短信验证码
	 * ZXX.comm.sendSms(valdCode,function(obj){
	 *	   o_o.getMessage(obj);
	 * });
	 */
	comm.sendSms = function(obj, fn) {
		// 初始化验证码插件
		obj.sendCode({
			disClass: 'btn-disabled',
			secs: 60,
			run: false,
			runStr: '{%s}秒后重新获取',
			resetStr: '重新获取验证码'
		});
		// 点击“获取验证码”
		obj.on("click", function() {
			fn(obj);
		});
	};
	/**
	 * 格式化金额
	 * @param {Float} s 金额
	 * @param {Number} n 保留小数，默认保留2两位
	 */
	comm.fmoney = function(s, n) {
		n = n > 0 && n <= 20 ? n : 2;
		s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
		var l = s.split(".")[0].split("").reverse(),
			r = s.split(".")[1],
			t = "";
		for(i = 0; i < l.length; i++) {
			t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
		}
		return t.split("").reverse().join("") + "." + r;
	}

	/**
	 * 输入延迟执行
	 * @param {Object} obj
	 * @param {Object} times
	 * @param {Object} fn
	 */
	comm.lazyExtn = function(obj, times, fn) {
		var wait;
		obj.on("input propertychang", function(e) {
			clearTimeout(wait);
			wait = setTimeout(function() {
				fn(obj.val())
			}, times);
		})
	};
    
    /**
    * 获取模板
    * @param {Object} path 模板路径
    * @param {Object} fn 回调函数
    */
    comm.getTpl = function(path,fn){
    	$.get(path,function(data){
    		fn(data);
    	});
    }
    /**
	 * 合并数据
	 * @param {Object} obj1 
	 * @param {Object} obj2 
	 * 保存obj1和obj2合并后产生新对象，若obj2中有与obj1相同的key，默认obj2将会覆盖obj1的值
	 */
    comm.mergeData = function(obj1,obj2){
    	return $.extend(true, obj1, obj2);
    }
   
	var statusCode = {
		'200': function(data, fn) {
			fn();
		},
		'401': function(data, fn) {
			dialog.toast("登陆过期，请重新登陆!", 'none', 1000);
			comm.toView('login.html');
		},
		'403': function(data, fn) {
			dialog.toast("系统检测到您的账号在另外一设备上登录，您在当前设备上登录的账号将自动退出。如非您本人操作，请尽快修改登录密码，防止信息泄露!", 'none');
			comm.toView('login.html');
		},
		'500': function(data, fn) {
			dialog.toast(data.msg, 'none',1000);
		}
	};
	
	/**
	 * 复制ajax状态码对象，便于自定义规则
	 */
	comm.ajaxStatus = function(){
		return $.extend({},statusCode);
	}

	/**
	 * 根据状态码进行统一处理
	 * @param {Object} data 接口数据对象
	 * @param {Function} fn 回调函数   【可选】
	 */
	comm.statusDo = function(data, fn) {
		return statusCode[data.status](data, fn || function() {});
	}

	/**
	 * 从url中解析token注入到session中
	 */
	comm.token = function() {
		var urltoken = comm.getParm("token");
		if(urltoken) {
			var login = comm.storage("login");
			if(login === "null") {
				login = {
					"token": urltoken
				};
			} else {
				login.token = urltoken;
			}
			comm.storage("login", login);
		}
	}

	// 自动判断是否登录
	comm.isLogin();

})(ZXX.comm = {});