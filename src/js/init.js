/**
 * 
 */
;(function(c){
	var location = window.location; // 当前页面的地址
	var protocol = location.protocol; // 协议
	var host = location.host; // 主机名
	
	// 协议和域名
    function getHostWithProtocol() {
        var origin = location.origin, host = location.host;
        if(!origin) {
            protocol = protocol === "http:" ? "http://" : protocol === "https:" ? "https://" : "file:///";
            origin = protocol + (host || location.hostname);
        }
        return origin;
    }
    
    // 获取根路径
    function getRootPath(){
	    var curWwwPath=window.document.location.href;
	    var pathName=window.document.location.pathname;
	    var pos=curWwwPath.indexOf(pathName);
	    var localhostPaht=curWwwPath.substring(0,pos);
	    var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1);
	    return(localhostPaht+projectName);
    }

	/**
	 * 加载一个脚本文件
	 * @param {String} url [文件路径]
	 * @param {Object} callback [回调函数]
	 */
	function _loadJsFile(url, callback) {
		var script = document.createElement("script");
		if(script.readyState) {
			script.onreadystatechange = function() {
				if(script.readyState == "loaded" || script.readyState == "complete") {
					callback.call();
				}
			}
		} else {
			script.onload = callback;
		}
		script.type = "text/javascript";
		script.async = true;
		script.src = url;
		document.getElementsByTagName("head")[0].appendChild(script);
	}

	/**
	 * 加载脚本文件列表
	 * @param {Object} urls
	 * @param {Object} statechange
	 * @param {Object} _index
	 */
	function _loadJsList(urls, statechange, _index) {
		var index = _index || 0;
		if(urls[index]) {
			_loadJsFile(urls[index], function() {
				_loadJsList(urls, statechange, index + 1);
			});
		}

		if(statechange) {
			statechange(index);
		}
	}

	var allHost = getHostWithProtocol();
	/**
	 * 根据域名解析文件url
	 * @param {Object} urls [资源路径集合]
	 * @param {Object} type [资源类型]
	 */
	function _parse(urls, type) {
		var _urls = [],
			url = "";

		if(typeof urls === "string") {
			urls = [urls];
		}

		for(var i = 0, len = urls.length; i < len; i++) {
			url = urls[i];
			if(!url) {
              return false;
			} else if(/^(http|https)/.test(url)) { // 完整的URL
				_urls.push(url);
			} else if(/^\//.test(url)) { // 以根目录为路径
				_urls.push(protocol + "//" + host + url);
			} else {
                _urls.push(getRootPath() + "/src/js/" + url);
			}
		}

		return _urls;
	}

	/**
	 * 加载js
	 * @param {Object} urls [资料路径集合]
	 * @param {Object} callback [回调函数]
	 * @param {Object} dontevent []
	 * @param {Object} showLoad []
	 * @param {Object} packJs []
	 */
	c.loadJs = function(urls, callback, dontevent, showLoad, packJs) {
		showLoading = showLoad;
		urls = _parse(urls, "js");
		if(packJs) {
			urls = _parse(packJs, "js");
		}
		if(!dontevent) {
			var _callback = callback,
				len = urls.length;
			callback = function(index) {
				if(_callback) {
					_callback(index);
				}
			}
		}
		_loadJsList(urls, callback);
	}

	c.loadCss = function() {

	}

	/**
	 * 获取设备信息
	 */
	c.device = function() {
		var doc = window.document,
			ua = window.navigator && window.navigator.userAgent || '';

		var ipad = !!ua.match(/(iPad).*OS\s([\d_]+)/),
			ipod = !!ua.match(/(iPod)(.*OS\s([\d_]+))?/),
			iphone = !ipad && !!ua.match(/(iPhone\sOS)\s([\d_]+)/);

		return {
			/**
			 * 是否移动终端
			 * @return {Boolean}
			 */
			isMobile: !!ua.match(/AppleWebKit.*Mobile.*/) || 'ontouchstart' in doc.documentElement,
			/**
			 * 是否IOS终端
			 * @returns {boolean}
			 */
			isIOS: !!ua.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
			/**
			 * 是否Android终端
			 * @returns {boolean}
			 */
			isAndroid: !!ua.match(/(Android);?[\s\/]+([\d.]+)?/),
			/**
			 * 是否ipad终端
			 * @returns {boolean}
			 */
			isIpad: ipad,
			/**
			 * 是否ipod终端
			 * @returns {boolean}
			 */
			isIpod: ipod,
			/**
			 * 是否iphone终端
			 * @returns {boolean}
			 */
			isIphone: iphone,
			/**
			 * 是否webview
			 * @returns {boolean}
			 */
			isWebView: (iphone || ipad || ipod) && !!ua.match(/.*AppleWebKit(?!.*Safari)/i),
			/**
			 * 是否微信端
			 * @returns {boolean}
			 */
			isWeixin: ua.indexOf('MicroMessenger') > -1,
			/**
			 * 是否火狐浏览器
			 */
			isMozilla: /firefox/.test(navigator.userAgent.toLowerCase()),
			/**
			 * 设备像素比
			 */
			pixelRatio: window.devicePixelRatio || 1
		};
	}
	
})(window.ZXX = {});
