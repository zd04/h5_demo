;
(function() {
	var dialog = window.YDUI.dialog;

	var o_o = {
		// 初始化
		init: function() {
			o_o.bindEvt();
		},
		// 绑定事件
		bindEvt: function() {

		},
		// 描述信息
		getPurchSaleThemeList:function(){
			var _data = JBY.comm.getAjaxParm();
 
			$.ajax({
				type: "get",
				url: JBY.cfg.getUrl('getPurchSaleThemeList'),
				data: _data,
				success: function(data) {
					dialog.loading.close();
                    JBY.comm.statusDo(data,function(){
                    	JBY.comm.log('描述信息');
                    	JBY.comm.log(data);
                    	if(data.data.length){
                    		// 渲染模板
                    		$(".g-scrollview").append(template('purchSaleThemeListTpl', data));
                    	}
                    });
				},
				error: function() {
					dialog.toast("网络异常", 'none', 1000);
					dialog.loading.close();
				}
			});
		}
	
	};

	o_o.init();
})();